# Listas de Capitais

![Some alt text](https://seeklogo.com/images/N/nuxt-logo-5EF50E1ABD-seeklogo.com.png)


### Ferramentas Utilizadas

- ##### [Nuxt.js](https://nuxtjs.org/)
- ##### [Typescript](https://typescript.nuxtjs.org/)
- ##### [Vuetify](https://next.vuetifyjs.com/en/)
- ##### [Jest](https://jestjs.io/)

Para o desenvolvimento e organização do código, foam utilizados:

- ##### Metodologia [BEM](http://getbem.com/introduction/) (Blocks, Elements and Modifiers)
- ##### Lib [Nuxt Property Decorator](https://www.npmjs.com/package/nuxt-property-decorator)
- ##### Mapeamento de funções do Vuex ([mapActions, mapGetters](https://blog.logrocket.com/a-complete-guide-to-mapping-in-vuex/))

### Visualização do Projeto

- ##### Pelo Link abaixo do deploy feito no Netlify:

[https://sad-stonebraker-5acc82.netlify.app](https://sad-stonebraker-5acc82.netlify.app)

#### ou

- #### Atráves da execução do projeto:
  - Clone o repositório
  - execute o comando `yarn install`
  - execute o comando `yarn dev`
  - Acesse o link padrão http://localhost:3000/ (caso dessa porta já esteja sendo utilizada, verificar o link gerado no terminal)

As listas foram separadas em `Getters`, Dessa forma a state não é modificada. Portando, a state pode ser reaproveitada em futuras implementações
